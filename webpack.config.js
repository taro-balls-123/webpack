const path = require('path');

// HtmlWebpackPlugin以某个html文件作为模版生成新的html文件 并把打包好的代码引入
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
  mode: 'production',
  devServer: {
    port: 1919
  },
  // 入口
  entry: "./src/main.js", 
  //出口
  output: {
    clean:true,//每次打包之前清空 dist 目录下的代码
    filename: 'bundle.js',//出口文件名
    path: path.join(__dirname + '/dist'),//出口路径
  },
  plugins: [new HtmlWebpackPlugin({
    template:'./public/index.html'//以对应的html文件作为模版生成新的html文件
  })],
  module: {
    rules: [//loader的规则
      {
        test: /\.css$/i,//要处理的文件   i忽略大小写   .css以css结尾的文件
        //css-loader的作用是让weback能够解析css文件
        //style-loader可以把解析好的样式插入到DOM中
        //数组中loader的使用顺序是从右到左
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.less$/i,
        use: [
          // compiles Less to CSS
          'style-loader',
          'css-loader',
          'less-loader',
        ],
      },
      {
        test: /\.(png|jpg|gif|jpeg)$/i,
        //asset 模式会把小于8kb 的转化为base64
        //把大于8kb的原图片输出
        type: 'asset'
      },
      {
        //不需要处理字体文件，直接源文件输出即可
        //所以使用的是asset/resource模式,会直接输出原文件

        test: /\.(svg|woff|woff2|eot|ttf)$/i,
        type: 'asset/resource',
        generator: {
          filename:'font/[name].[hash:6][ext]'//[name]原来的名字，[harsh:6]六位的字符串 ，[ext]原来的扩展名
        }
      },
      //babel
      {
        test: /\.js$/i,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ],
  },
}

// console.log(123);
//引入jquery
import $ from 'jquery'

//引入css文件
import './css/index.css'
//引入less
import './less/index.less'
//引入小于8kb图片
import logo from './assets/logo_small.png'
//引入大于8kb图片
import run from './assets/1.gif'
//引入iconfont.css
import './assets/fonts/iconfont.css'



$('#myUL li:odd').css('color','pink')
$('#myUL li:even').css('color','blue')
//创建标签
const img1 = document.createElement('img')
img1.src = logo
document.body.appendChild(img1)

const img2 = document.createElement('img')
img2.src = run
document.body.appendChild(img2)


//创建标签
const a = document.createElement('i')
//添加类名
a.className = 'iconfont icon-weixin'
//追加
document.body.appendChild(a)

const b = document.createElement('i')
b.className = 'iconfont icon-qq'
document.body.appendChild(b)

const fff = () => {
  console.log('啊啊啊');
}
console.log(fff);
fff()
console.log('222');